# How to install Grafana on Raspberry Pi  

wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -  
echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list  
sudo apt update  
sudo apt install grafana  
sudo /bin/systemctl daemon-reload  
sudo /bin/systemctl enable grafana-server  
sudo /bin/systemctl start grafana-server  
sudo /bin/systemctl status grafana-server  

# Interface Grafana  

Turn off Domain Firewall on Windown

http://<RASPBERRYPI_IP>:3000  
Use the default credentials: admin / admin.  


