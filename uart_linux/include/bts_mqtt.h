/**
 * @file mqtt.h
 * @author long (longzil193@gmail.com)
 * @brief mqtt uart header file
 * @version 0.1
 * @date 2023-02-06
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#ifndef _MQTT_
#define _MQTT_

#include "main.h"

#define MQTT_HOST           "192.168.222.112"
//#define MQTT_HOST           "Remote.mysignage.vn"
#define MQTT_PORT           1883
#define CLIENT_ID_1         "uart_mqtt_1"
#define CLIENT_ID_2         "uart_mqtt_2"

#define SUB_OTA_UART            "gw/ota/uart"
#define SUB_UART_CONTROL        "gw/uart/control"
#define PUB_LOG_UART            "gw/log/uart"
#define PUB_DEVICE_UPDATE_UART  "gw/device/update/uart"

enum topics {   
    GW_OTA_UART = 0,
    GW_UART_CONTROL,
};

/*------------------------------------------------------------------------------*/

class mqtt
{
    private:
        struct mosquitto    *mosq = NULL;
        const char          *id;
        const char          *host;
        int                 port;
        int                 keepalive;

    public:
        mqtt(const char *id, const char *host, int port = 1883, int keepalive = 60);
        ~mqtt();
        struct mosquitto* getCLIENT();
        void loop_write();
        void loop_read();
        void run(void);
        void rerun(void);
        void send(const char * topic, const char * msg);
        void subscribe(const char * topic);
        std::string check(int err);
};

/*------------------------------------------------------------------------------*/

int Topic_converter( std::string);

/*------------------------------------------------------------------------------*/
#endif /* MQTT */
