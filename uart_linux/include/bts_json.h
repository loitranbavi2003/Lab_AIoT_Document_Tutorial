/**
 * @file json.h
 * @author long (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-02-20
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#pragma once
#ifndef _BTSJSON_H_
#define _BTSJSON_H_

#include "main.h"

#define JSON_KEY_1              "position"
#define JSON_KEY_2              "field"
#define JSON_KEY_3              "value"

#define JSON_VALUE_2            "value"

#define DEVICE_OFFSET           50
#define SENSOR_OFFSET           150

Json::Value parseJSON(std::string input);
const char* Create_sensor_float_message( float, uint8_t );
const char* Create_device_integer_message( uint8_t, uint8_t );

#endif