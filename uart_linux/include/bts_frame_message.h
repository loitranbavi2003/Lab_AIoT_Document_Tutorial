
#ifndef _FRAME_MESSAGE_H_
#define _FRAME_MESSAGE_H_


#include "main.h"

/* Byte bắt đầu (mặc định) */
#define START_BYTE 0xAA55

/* Độ dài mặc định không đổi [Start(2byte) + TypeMessage(2byte) + Length(2byte) = 6byte] */
#define DEFAULT_BYTE                            6      
// Check sum using 2byte default
#define DEFAULT_BYTE_CHECKSUM                   2          
// support max up to 10 sensors
#define DEFAULT_MAX_NUMBER_SENSOR               10         
#define DEFAULT_MAX_NUMBER_DEVICE               (DEFAULT_MAX_NUMBER_SENSOR*4)

// Number byte using for control Device. List Device: 1byte, Value: 1 byte - 0 to 255
#define DEFAULT_BYTE_CONTROL_DEVICE             2

/**
 * @brief 
 * messageFrameMsg_t used to create a standard data frame for the message.
 */
typedef struct
{
    uint16_t Start;
    uint16_t TypeMessage;
    uint16_t Length;
    uint8_t Data[(DEFAULT_MAX_NUMBER_SENSOR*4)];
    uint16_t Crc;
}messageFrameMsg_t;

/**
 * @brief 
 * Number of sensors available
 */
#define SIZE_LIST_SENSOR 6

/**
 * @brief 
 * Number of sensors available
 */
typedef enum 
{
    SENSOR_NTC1 				 = 0,
    SENSOR_NTC2 				    ,
    SENSOR_DOOR 				    ,
    SENSOR_SMOKE 				    ,
    SENSOR_WATER 				    ,
    SENSOR_CONDITIONER_STATUS 	    ,
}listSensor_e;

#define SIZE_LIST_DEVICE 3
typedef enum 
{
    DEVICE_CONDITIONER 		        = 0x01,
    DEVICE_FAN 				        = 0x02,
    DEVICE_LAMP 				    = 0x03,
}listDevice_e;

#define TYPE_MESSAGE_NUMBER 3
typedef enum
{
    TYPE_MESSAGE_UPDATE_SENSOR 	    = 0x0001,
    TYPE_MESSAGE_UPDATE_DEVICE 	    = 0x0002,
    TYPE_MESSAGE_CONTROL_DEVICE 	= 0x0003,
} typeMessageSensor_e;

class FrameMessage
{
    
public:
	int16_t CreateMessage(messageFrameMsg_t datain, uint8_t *dataout);
    int16_t DetectMessage(uint8_t *dataint, messageFrameMsg_t *dataout);
    int16_t GetTypeMessage(uint8_t *dataint);
private:
	uint16_t CheckSum(uint8_t *buf, uint8_t len);
};

#endif 
