
#ifndef _SYSTEM_H_
#define _SYSTEM_H_

#include "main.h"

#define PortSerialName      "/dev/ttyHSL1"
#define BaudRateSerial      B9600

class System
{
    public:
        int8_t SystemInit(void);
        int16_t CreateControlDeviceMassage(const uint8_t typedevice, uint8_t valuce, uint8_t *arr);
        int16_t GetDataSensor(uint8_t *datain, float *dataout);
        int16_t GetDataDevice(uint8_t *datain, uint8_t *dataout);
    public:
};

#endif 
