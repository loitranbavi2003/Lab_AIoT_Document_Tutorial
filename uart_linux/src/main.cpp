/**
   @file main.cpp
   @author long (longzil193@gmail.com)
   @brief
   @version 0.1
   @date 2023-02-16

   @copyright Copyright (c) 2023

*/
#include "main.h"

Json::Reader reader;
Json::Value  root;

SerialPort      serial(PortSerialName, BaudRateSerial, 1);
DebugLogger     DebugLog;
GetMessage      getmessage;
FrameMessage    framemessage;
System          Message;

uint8_t         CreateFrame_Test(uint8_t *arr);
uint16_t        CountTimeCheckConnectMQtt = 0;
std::string     message_payload_str;
std::string     message_topic;
int             position;
int             value;
const char      *message_payload_cstr;
static int      run = 1;

mqtt            mqttClient1(CLIENT_ID_1, MQTT_HOST, MQTT_PORT); // client for publish
mqtt            mqttClient2(CLIENT_ID_2, MQTT_HOST, MQTT_PORT); // client for subscribe & callbacks

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message) {
  message_payload_str = (char*) message->payload;
  message_topic = message->topic;
  DebugLog.DebugLOG(MQTT_LOCAL, {
    "Recieved message",
    "Topic:" + (std::string)message->topic,
    "Content:" + message_payload_str
  });
  uint32_t count_message = 0;
  uint8_t array[40];
  switch (Topic_converter(message_topic))
  {
    case GW_OTA_UART:
      message_payload_cstr = message_payload_str.c_str();
      mqttClient2.send(PUB_LOG_UART, message_payload_cstr);
      break;

    case GW_UART_CONTROL:
      root = parseJSON(message_payload_str);
      position = root[JSON_KEY_1].asInt();

      if (position < 150)
        position -= 50;
      else
        position -= 150;

      value = root[JSON_KEY_3].asInt();
      try {
        count_message = Message.CreateControlDeviceMassage(position, value, array);
        if (count_message > 0)
        {
          std::cout << std::endl;
          serial.writebyte(array, count_message);
        }
        else
        {
          std::cerr << "Create Control Writebyte Error" << std::endl;
        }
      }
      catch (const std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
      }
      break;
    default:
      break;
  }
}

int main()
{
  std::cout << "UART Starts" << std::endl;

  uint8_t array[100];
  uint32_t count_message = 0;
  uint16_t sizeof_message = 0;

  count_message = 0;

  mqttClient1.run();
  mqttClient2.run();
  mqttClient2.subscribe(SUB_UART_CONTROL);
  mqttClient2.subscribe(SUB_OTA_UART);
  //mqttClient2.subscribe(SUB_PING);
  mosquitto_message_callback_set(mqttClient2.getCLIENT(), message_callback);

  while (run)
  {
    int32_t checkdata = serial.Available();
    if (checkdata > 0)
    {
      getmessage.GetMessages(serial.readbyte(), array);
      if (getmessage.IsMessage(sizeof_message) == 1)
      {
        float array_float[10];
        uint8_t array_uint[40];
        int16_t TypeMessage = framemessage.GetTypeMessage(array);
        int16_t size_data = 0;
        if (sizeof_message > 0)
        {
          if (TypeMessage == TYPE_MESSAGE_UPDATE_DEVICE)
          {
            size_data = Message.GetDataDevice(array, array_uint);
            DebugLog.DebugArrDevice(array_uint, size_data);
            for (int i = 0; i < DEFAULT_MAX_NUMBER_DEVICE; i++)
            {
              try {
                message_payload_cstr = Create_device_integer_message(array_uint[i], i);
              }
              catch (const std::exception& e) {
                std::cerr << "Error: " << e.what() << std::endl;
              }
              mqttClient1.send( PUB_DEVICE_UPDATE_UART, message_payload_cstr);
              mqttClient1.loop_write();
            }
          }
          else if (TypeMessage == TYPE_MESSAGE_UPDATE_SENSOR)
          {
            size_data = Message.GetDataSensor(array, array_float);
            DebugLog.DebugArrSensor(array_float, size_data);
            for (int i = 0; i < DEFAULT_MAX_NUMBER_SENSOR; i++)
            {
              try {
                message_payload_cstr = Create_sensor_float_message(array_float[i], i);
              }
              catch (const std::exception& e) {
                std::cerr << "Error: " << e.what() << std::endl;
              }
              mqttClient1.send( PUB_DEVICE_UPDATE_UART, message_payload_cstr);
              mqttClient1.loop_write();
            }
          }
          printf("\n-------------------------- (%d) -------------------------- \n", sizeof_message);
          //DebugLog.DebugFrameMessage(array);
          printf("-------------------------- (END: %d) -------------------------- \n", count_message);
          count_message++;
        }
      }
    }
    if (CountTimeCheckConnectMQtt >= 3000)
    {
      mosquitto_loop(mqttClient2.getCLIENT() , 0, 1);
      CountTimeCheckConnectMQtt = 0;
    }
    usleep(1000);
    CountTimeCheckConnectMQtt++;
    getmessage.TimeOut();
  }
  mqttClient1.~mqtt();
  mqttClient2.~mqtt();
  return 0;
}

