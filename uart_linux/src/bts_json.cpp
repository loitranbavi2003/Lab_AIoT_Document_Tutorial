#include "bts_json.h"

static Json::Reader     reader;
static Json::Value      root;
static Json::FastWriter writer;
static std::string      message_payload_str;

/**
   @brief Json Parser function

   @param input
   @return Json::Value
*/
Json::Value parseJSON(std::string input) {
  reader.parse(input, root);
  return root;
}

/**
   @brief Create sensor messages

   @param dataf
   @param start_index
   @return const char
*/
const char* Create_sensor_float_message( float dataf, uint8_t start_index)
{
  DebugLogger   DebugLog;
  root[JSON_KEY_1] = start_index + 1 + SENSOR_OFFSET;
  root[JSON_KEY_2] = JSON_VALUE_2;
  root[JSON_KEY_3] = dataf;

  message_payload_str = writer.write(root);

  static char message_payload_cstr[128];
  strcpy(message_payload_cstr,  message_payload_str.c_str());
  DebugLog.DebugLOG(UART_DATA, {
    "Type: Sensor",
    "Content: " + message_payload_str
  });
  return message_payload_cstr;
}

/**
   @brief Create device messages

   @param dataI
   @param start_index
   @return const char
*/
const char* Create_device_integer_message( uint8_t dataI, uint8_t start_index)
{
  DebugLogger   DebugLog;
  root[JSON_KEY_1] = start_index + 1 + DEVICE_OFFSET;
  root[JSON_KEY_2] = JSON_VALUE_2;
  root[JSON_KEY_3] = dataI;

  message_payload_str = writer.write(root);
  DebugLog.DebugLOG(UART_DATA, {
    "Type: Device",
    "Content: " + message_payload_str
  });
  static char message_payload_cstr[128];
  strcpy(message_payload_cstr,  message_payload_str.c_str());
  return message_payload_cstr;
}