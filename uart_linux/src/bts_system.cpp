#include "bts_system.h"


static float Bytes_To_Float(uint8_t data0, uint8_t data1, uint8_t data2, uint8_t data3)
{
  data_format_float_bytes input_convert;
  input_convert.bytes[3] = data0;
  input_convert.bytes[2] = data1;
  input_convert.bytes[1] = data2;
  input_convert.bytes[0] = data3;

  return input_convert.data_float;
}


int8_t System::SystemInit(void)
{
  return RETRUN_ERROR;
}

/**
 * @brief Creates a control message for a device and stores it in the specified byte array.
 *
 * This function creates a control message for a device of the specified type and with the specified
 * value, and stores the message in the specified byte array. The length of the message is returned.
 * If an error occurs, such as if the specified device type is invalid or if the byte array is not
 * large enough to store the message, this function returns -1.
 *
 * @param typedevice The type of the device to create a control message for.
 * @param value The value to set for the device.
 * @param arr The byte array in which to store the control message.
 * @return The length of the control message stored in the byte array, or -1 if an error occurs.
 */
int16_t System::CreateControlDeviceMassage(const uint8_t typedevice, uint8_t valuce, uint8_t *arr)
{
  int16_t size_data;
  messageFrameMsg_t datain;
  FrameMessage framemessage;
  DebugLogger Debug;
  /*---------------------------------(Start)---------------------------------*/
  datain.Start = START_BYTE;
  /*---------------------------------(TypeMessage)---------------------------------*/
  datain.TypeMessage = TYPE_MESSAGE_CONTROL_DEVICE;
  /*---------------------------------(Data)---------------------------------*/
  datain.Data[0] = typedevice;
  datain.Data[1] = valuce;
  /*---------------------------------(Data)---------------------------------*/
  size_data = framemessage.CreateMessage(datain, arr);

  if (size_data > 0)
  {
    for (int i = 0; i < size_data; i++)
    {
      if (arr[i] <= 0x0f)
      {
        printf("0%x ", arr[i]);
      }
      else
      {
        printf("%x ", arr[i]);
      }
    }
    std::cout << std::endl;
    Debug.DebugStatus("System::CreateControlDeviceMassage - Create done");
    return size_data;
  }
#if ENABLE_DEBUG_SYSTEM_ERROR
  Debug.DebugError("System::CreateControlDeviceMassage - Create Error");
#endif
  return RETRUN_ERROR;
}

/**
 * @brief Converts a byte array of sensor data into floating-point values.
 *
 * This function converts a byte array containing sensor data into floating-point values, and stores
 * the results in the specified floating-point array. The function assumes that the byte array has
 * been formatted according to a specific protocol, which is documented elsewhere. If an error occurs
 * during conversion, such as if the byte array is not the correct length or contains invalid data,
 * this function returns -1.
 *
 * @param datain The byte array containing the sensor data to convert.
 * @param dataout The floating-point array in which to store the converted values.
 * @return The number of floating-point values converted and stored in the output array, or -1 if an
 * error occurs.
 */
int16_t System::GetDataSensor(uint8_t *datain, float *dataout)
{
  DebugLogger Debug;
  messageFrameMsg_t dataoutemp;
  FrameMessage framemessage;
  uint16_t count_arr = 0;
  int16_t size_data = framemessage.DetectMessage(datain, &dataoutemp);
  if (size_data > 0)
  {
    if (dataoutemp.TypeMessage == TYPE_MESSAGE_UPDATE_SENSOR)
    {
      for (int i = 0; i < ((size_data - 8) / 4); i++)
      {

        dataout[i] =  Bytes_To_Float(dataoutemp.Data[count_arr], dataoutemp.Data[count_arr + 1], dataoutemp.Data[count_arr + 2], dataoutemp.Data[count_arr + 3]);
        count_arr += 4;
      }
    }
  }
  else
  {
#if ENABLE_DEBUG_SYSTEM_ERROR
    Debug.DebugError("System::GetDataSensor - size_data < 0");
#endif
    return RETRUN_ERROR;
  }
  return (count_arr / 4);
}

/**
 * @brief Converts a byte array of device data into unsigned integer values.
 *
 * This function converts a byte array containing device data into unsigned integer values, and stores
 * the results in the specified integer array. The function assumes that the byte array has
 * been formatted according to a specific protocol, which is documented elsewhere. If an error occurs
 * during conversion, such as if the byte array is not the correct length or contains invalid data,
 * this function returns -1.
 *
 * @param datain The byte array containing the device data to convert.
 * @param dataout The integer array in which to store the converted values.
 * @return The number of integer values converted and stored in the output array, or -1 if an
 * error occurs.
 */
int16_t System::GetDataDevice(uint8_t *datain, uint8_t *dataout)
{
  DebugLogger Debug;
  messageFrameMsg_t dataoutemp;
  FrameMessage framemessage;
  uint16_t count_arr = 0;
  int16_t size_data = framemessage.DetectMessage(datain, &dataoutemp);
  if (size_data > 0)
  {
    if (dataoutemp.TypeMessage == TYPE_MESSAGE_UPDATE_DEVICE)
    {
      for (int i = 0; i < ((size_data - 8)); i++)
      {
        dataout[i] =  dataoutemp.Data[i];
        count_arr++;
      }
    }
  }
  else
  {
#if ENABLE_DEBUG_SYSTEM_ERROR
    Debug.DebugError("System::GetDataDevice - size_data < 0");
#endif
    return RETRUN_ERROR;
  }
#if ENABLE_DEBUG_SYSTEM_STATUS
  Debug.DebugStatus("System::GetDataDevice - Get data Done");
#endif
  return (count_arr);
}

