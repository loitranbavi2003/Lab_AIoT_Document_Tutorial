
#include "bts_frame_message.h"

uint8_t array_size_data_sensor[SIZE_LIST_SENSOR] = {4, 4, 1, 1, 1, 1};

/**
 * @brief Convert two 8-bit unsigned integers into a 16-bit unsigned integer
 * 
 * Given two 8-bit unsigned integers, this function returns a 16-bit unsigned
 * integer that is the concatenation of the two input values. The first input value
 * is stored in the least significant byte of the output, while the second input
 * value is stored in the most significant byte of the output.
 * 
 * @param data1 The least significant byte of the input value
 * @param data2 The most significant byte of the input value
 * @return The 16-bit unsigned integer resulting from the concatenation of the input values
 */
static uint16_t Bytes_To_Uint16(uint8_t data1, uint8_t data2)
{
  data_format_uint8_16_t input_convert;
  input_convert.bytes[0] = data1;
  input_convert.bytes[1] = data2;

  return input_convert.data_uint16;
}

/**
 * @brief Convert four 8-bit unsigned integers into a 32-bit floating point number
 * 
 * Given four 8-bit unsigned integers, this function returns a 32-bit floating point
 * number that is created by concatenating the four input values in little-endian order.
 * That is, the least significant byte of the output is data3, followed by data2, data1,
 * and finally data0, which is the most significant byte of the output.
 * 
 * @param data0 The most significant byte of the input value
 * @param data1 The second most significant byte of the input value
 * @param data2 The second least significant byte of the input value
 * @param data3 The least significant byte of the input value
 * @return The 32-bit floating point number resulting from the concatenation of the input values
 */
static float Bytes_To_Float(uint8_t data0, uint8_t data1, uint8_t data2, uint8_t data3)
{
  data_format_float_bytes input_convert;
  input_convert.bytes[3] = data0;
  input_convert.bytes[2] = data1;
  input_convert.bytes[1] = data2;
  input_convert.bytes[0] = data3;
  return input_convert.data_float;
}

/**
 * @brief Creates a message frame from a message structure
 * 
 * Given a message structure, this function creates a message frame that can be sent
 * over a communication channel. The message frame includes a header and a checksum,
 * as well as the message data itself. The output message frame is stored in the
 * provided dataout buffer.
 * 
 * @param datain The message structure to be converted into a message frame
 * @param dataout The buffer where the resulting message frame will be stored
 * @return The length of the resulting message frame in bytes, or a negative error code if the conversion fails
 */
int16_t FrameMessage::CreateMessage(messageFrameMsg_t datain, uint8_t *dataout)
{
  uint8_t *data_sensor_temp;
  uint16_t count_arr = 0;
  messageFrameMsg_t *frame_message_temp;
  DebugLogger Debug;
  if (datain.Start != START_BYTE)
  {
#if ENABLE_DEBUG_FRAME_MESSAGE_ERROR
    Debug.DebugError("FrameMessage::CreateMessage - Start Error");
#endif
    return RETRUN_ERROR;
  }

  /*dùng con trỏ frame_sensor_temp trỏ đến Sensor_DataIn*/
  frame_message_temp = &datain;
  /*ép kiểu frame_sensor_temp về kiểu uint8_t, mục đích để chuyển từ struct -> arr*/
  data_sensor_temp = (uint8_t *)frame_message_temp;

  switch (datain.TypeMessage)
  {

    case TYPE_MESSAGE_UPDATE_SENSOR:
    case TYPE_MESSAGE_UPDATE_DEVICE:
      /**
         @brief
         if type message is updated sensor -> length is 40bytes because supported max is 10 sensors, one sensor using 4 bytes(float).
         if type message is updated device -> length is 40bytes because supported max is 40 Devices, one device using 1 bytes(unsigned char).
      */
      datain.Length =  (DEFAULT_MAX_NUMBER_SENSOR * 4) + DEFAULT_BYTE_CHECKSUM;
      break;

    case TYPE_MESSAGE_CONTROL_DEVICE:
      /**
           @brief
           if type message is control device -> length is 2 bytes
           1 byte for Name Device
           1 byte for value control
      */
      datain.Length =  DEFAULT_BYTE_CONTROL_DEVICE + DEFAULT_BYTE_CHECKSUM;
      break;
    default:
#if ENABLE_DEBUG_FRAME_MESSAGE_ERROR
      Debug.DebugError("FrameMessage::CreateMessage - TypeMessage Error");
#endif
      return RETRUN_ERROR;
      break;
  }

  datain.Crc = CheckSum(data_sensor_temp, (DEFAULT_BYTE + (datain.Length - DEFAULT_BYTE_CHECKSUM)));

  for (count_arr = 0; count_arr < (DEFAULT_BYTE + datain.Length - DEFAULT_BYTE_CHECKSUM); count_arr++)
  {
    dataout[count_arr] = data_sensor_temp[count_arr];
  }
  /*Ghi giá trị checksum tính được vào cuối mảng*/
  dataout[count_arr]      = (datain.Crc & 0xff);
  dataout[count_arr + 1]  = ((datain.Crc >> 8) & 0xff);
  /*tăng giá trị mảng lên 2 lần vì đã thêm ở trên*/
  count_arr += 2;
#if ENABLE_DEBUG_FRAME_MESSAGE_STATUS
  Debug.DebugStatus("FrameMessage::CreateMessage - Create Done");
#endif
  return count_arr;
}

/**
 * @brief Detects a message frame and extracts the message data
 * 
 * Given a buffer of data, this function checks whether the data contains a valid
 * message frame, and if so, extracts the message data from the frame and stores it
 * in a message structure. The function returns the length of the message frame in
 * bytes, or a negative error code if the frame is invalid or incomplete.
 * 
 * @param dataint The input buffer containing the data to be checked
 * @param dataout The message structure where the resulting message data will be stored
 * @return The length of the message frame in bytes, or a negative error code if the frame is invalid or incomplete
 */
int16_t FrameMessage::DetectMessage(uint8_t *dataint, messageFrameMsg_t *dataout)
{
  DebugLogger Debug;
  uint8_t count_temp = 0;
  messageFrameMsg_t *frame_message_temp;
  frame_message_temp = (messageFrameMsg_t*)dataint;
  /*---------------------------------(Start (2byte))---------------------------------*/
  dataout->Start = Bytes_To_Uint16(dataint[count_temp], dataint[count_temp + 1]);
  if (dataout->Start != START_BYTE)
  {
#if ENABLE_DEBUG_FRAME_MESSAGE_ERROR
    Debug.DebugError("FrameMessage::DetectMessage - Start Error");
#endif
    return RETRUN_ERROR;
  }
  count_temp += 2;

  /*---------------------------------(Type Message (2byte))---------------------------------*/
  dataout->TypeMessage = Bytes_To_Uint16(dataint[count_temp], dataint[count_temp + 1]);
  count_temp += 2;
  switch (dataout->TypeMessage)
  {
    case TYPE_MESSAGE_UPDATE_SENSOR:
    case TYPE_MESSAGE_UPDATE_DEVICE:
      /*---------------------------------(Length Udate (2byte))---------------------------------*/
      dataout->Length =  Bytes_To_Uint16(dataint[count_temp], dataint[count_temp + 1]);
      count_temp += 2;

      /*---------------------------------(Data (40byte))---------------------------------*/
      for (int i = 0; i < (dataout->Length - 2); i++)
      {
        dataout->Data[i] = dataint[count_temp];
        count_temp ++;
      }
      break;

    case TYPE_MESSAGE_CONTROL_DEVICE:
      /*---------------------------------(Length Udate (2byte))---------------------------------*/
      dataout->Length =  Bytes_To_Uint16(dataint[count_temp], dataint[count_temp + 1]);
      count_temp += 2;

      /*---------------------------------(Data (40byte))---------------------------------*/
      for (int i = 0; i < (dataout->Length - 2); i++)
      {
        if (i <= 1)
        {
          dataout->Data[i] = dataint[count_temp];
        }
        else
        {
          dataout->Data[i] = 0x00;
        }
        count_temp ++;
      }
      break;
    default:
#if ENABLE_DEBUG_FRAME_MESSAGE_ERROR
      Debug.DebugError("FrameMessage::DetectMessage - TypeMessage Error");
#endif
      return RETRUN_ERROR;
      break;
  }
  /*---------------------------------(Check Sum(2byte))---------------------------------*/
  uint16_t check_sum = CheckSum(dataint, (DEFAULT_BYTE + (dataout->Length - DEFAULT_BYTE_CHECKSUM)));
  dataout->Crc = Bytes_To_Uint16(dataint[count_temp], dataint[count_temp + 1]);

  if (check_sum != dataout->Crc)
  {
#if ENABLE_DEBUG_FRAME_MESSAGE_ERROR
    Debug.DebugError("FrameMessage::DetectMessage - CheckSum Error");
#endif
    return RETRUN_ERROR;
  }
  count_temp += 2;
#if ENABLE_DEBUG_FRAME_MESSAGE_STATUS
  Debug.DebugStatus("FrameMessage::DetectMessage - Detect Done");
#endif
  return count_temp;
}

/**
 * @brief Extracts the message type from a message frame
 *
 * This function takes a buffer of data that contains a message frame and extracts
 * the message type from it. The message type is stored in the first byte of the
 * frame and indicates what kind of message is being sent. The function returns
 * the message type code as an integer value.
 *
 * @param dataint Pointer to the buffer containing the message frame data
 * @return The message type code, or a negative error code if the frame is invalid or incomplete
 */
int16_t FrameMessage::GetTypeMessage(uint8_t *dataint)
{
  DebugLogger Debug;
  int16_t type_msg = Bytes_To_Uint16(dataint[2], dataint[3]);
  if (type_msg > TYPE_MESSAGE_CONTROL_DEVICE)
  {
#if ENABLE_DEBUG_FRAME_MESSAGE_ERROR
    Debug.DebugError("FrameMessage::GetTypeMessage - TypeMessage Error");
#endif
    return RETRUN_ERROR;
  }
#if ENABLE_DEBUG_FRAME_MESSAGE_STATUS
  if (type_msg == TYPE_MESSAGE_UPDATE_DEVICE)
  {
    Debug.DebugStatus("FrameMessage::GetTypeMessage - UPDATE DEVICE");
  }
  else if (type_msg == TYPE_MESSAGE_UPDATE_SENSOR)
  {
    Debug.DebugStatus("FrameMessage::GetTypeMessage - UPDATE SENSOR");
  }
  else if (type_msg == TYPE_MESSAGE_CONTROL_DEVICE)
  {
    Debug.DebugStatus("FrameMessage::GetTypeMessage - CONTROL DEVICE");
  }
#endif
  return type_msg;
}

/**
 * @brief Calculates the checksum of a buffer of data
 *
 * This function takes a buffer of data and its length and calculates the checksum
 * value of the data. The checksum is computed as the 16-bit ones' complement of the
 * sum of all the 16-bit words in the data. The function returns the checksum value
 * as a 16-bit unsigned integer.
 *
 * @param buf Pointer to the buffer containing the data to be checksummed
 * @param len The length of the data in bytes
 * @return The checksum value as a 16-bit unsigned integer
 */
uint16_t FrameMessage::CheckSum(uint8_t *buf, uint8_t len)
{
  uint16_t crc = 0xFFFF, pos = 0, i = 0;
  for (pos = 0; pos < len; pos++)
  {
    crc ^= (uint16_t)buf[pos]; // XOR byte into least sig. byte of crc
    for (i = 8; i != 0; i--)   // Loop over each bit
    {
      if ((crc & 0x0001) != 0) // If the LSB is set
      {
        crc >>= 1; // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else // Else LSB is not set
      {
        crc >>= 1; // Just shift right
      }
    }
  }
  return crc;
}
